# Add wireguard host

Script to configure a new [wireguard](https://www.wireguard.com/) host and tunnel on GNU/Linux (maybe other *nix too)

## Overview

This script creates client and server configuration files for wireguard.
It does not reload wireguard, you have to do that yourself.
It does not deploy the generated files to your clients nor to your server, 
except when the server is localhost and stars align (e.g. BASEDIR is the 
real wireguard configuration directory, see help).

The **goal** is to make the process of creating **wireguard** **configuration** files 
**less** **error-prone** than it is with manual editing.

## Install

This script has been briefly tested on Debian GNU/Linux with bash, 
wireguard-tools and a few other rather common utilities for a standard
Debian GNU/Linux system (cat, cut, iconv, grep, md5sum, sed, tee, 
maybe a few others). It works for me and I suppose it can work 
out-of-the-box on many other GNU/Linux or even *nix systems, but YMMV.

Once you've installed dependencies (*wireguard-tools* on Debian or any other
package that provides the *wg* command on other distros), you just have to
download the *addwghost* file, save it somewhere in your $PATH and make it
executable. Here is one way to do that in a Bash shell:

    avgjoe@linuxhost:~$ mkdir ~/bin
    avgjoe@linuxhost:~$ cd bin
    avgjoe@linuxhost:~/bin$ wget 'https://gitlab.com/lucrus/add-wireguard-host/-/raw/master/addwghost'
    avgjoe@linuxhost:~/bin$ chmod +x addwghost

## Running the script

Well, this is a matter of opening a terminal and issuing the command *addwghost*.
That's it: the script is self documented and will lead you to know how to use it
for your use case, but it will assume some defaults that might not suit everyone. 
If you want to read its full docs beforehand, just try *addwghost -h*.

If you rather prefer a fairly complete, ready to use example, here's one, but 
remember to change at least the FQDN with your real WireGuard server hostname,
and maybe the listen port (add `-l XXXXX` option where XXXXX is the UDP port) 
to match any firewall rules you mave have set up at your server. Feel free to 
change IPv4 and IPv6 classes with any other non routable IP classes you may like:

    avgjoe@linuxhost:~$ mkdir wireguard_configs
    avgjoe@linuxhost:~$ addwghost -e wg-vpn-host.example.com -n myTrustworthyNotebook -a -b ~/wireguard_configs -4 10.42.42 -6 fc00:42:42:42:42:42:42
    VPN name not set, generating one from wg-vpn-host.example.com
    Using wgvpnhostexampl as VPN name
    No client IP address suffix specified: looking up the first unused one
    No listen port specified, using 12345 as default
    Creating initial /home/avgjoe/wireguard_configs/wgvpnhostexampl.conf
    Saving client private key into /home/avgjoe/wireguard_configs/keys/wgvpnhostexampl/myTrustworthyNotebook.key
    Saving client public key into /home/avgjoe/wireguard_configs/keys/wgvpnhostexampl/myTrustworthyNotebook.pub
    Enabling new client: updating /home/avgjoe/wireguard_configs/wgvpnhostexampl.conf and its copy /home/avgjoe/wireguard_configs/keys/wgvpnhostexampl/wireguard.conf
    You should reload the wireguard daemon to make it read the new configuration file
    Creating client configuration file /home/avgjoe/wireguard_configs/keys/wgvpnhostexampl/myTrustworthyNotebook.conf
    You should deliver this file to the client in a secure manner
    
    # Auto generated addwghost configuration file created on 2022-12-24_12:24:08
    # You can save this by copy-and-paste as configuration file for future use.
    # If you plan to have only one tunnel, you can use one of the deafult names
    # for this file, such as /home/avgjoe/.config/addwghostrc.
    
    AUTO_CLIENT_IP_SUFFIX_LOOKUP=1
    LISTEN_PORT=12345
    VPN_NAME="wgvpnhostexampl"
    VPN_ENDPOINT="wg-vpn-host.example.com"
    BASEDIR="/home/avgjoe/wireguard_configs"
    SERVER_IPv4_PREFIX="10.42.42"
    SERVER_IPv6_PREFIX="fc00:42:42:42:42:42:42"

Now you can copy the last lines of the output into `~/.config/addwghostrc`, 
so that future invocations of the script, that you may run to create new client 
configurations for the same VPN tunnel, will use the same options of this first one,
without specifying them on the command line each time. E.g. in future you would be
able to just issue:

    avgjoe@linuxhost:~$ addwghost -n myNewNotebook
    Including /home/avgjoe/.config/addwghostrc
    No client IP address suffix specified: looking up the first unused one
    Saving client private key into /home/avgjoe/wireguard_configs/keys/wgvpnhostexampl/myNewNotebook.key
    Saving client public key into /home/avgjoe/wireguard_configs/keys/wgvpnhostexampl/myNewNotebook.pub
    Enabling new client: updating /home/avgjoe/wireguard_configs/wgvpnhostexampl.conf and its copy /home/avgjoe/wireguard_configs/keys/wgvpnhostexampl/wireguard.conf
    You should reload the wireguard daemon to make it read the new configuration file
    Creating client configuration file /home/avgjoe/wireguard_configs/keys/wgvpnhostexampl/myNewNotebook.conf
    You should deliver this file to the client in a secure manner

All done. You only have to copy `~/wireguard_configs/wgvpnhostexampl.conf` over into 
your WireGuard server `/etc/wireguard` directory, and deploy the client configuration 
files (i.e. `~/wireguard_configs/keys/wgvpnhostexampl/myNewNotebook.conf`) to your clients. 

Installing WireGuard on the server and on the clients is beyond the scope of this tool,
but it is usually an easy task.
